﻿using System;
using System.Data.Entity;

namespace rushinc.Model
{
    public abstract class ApiResponse : DbContext
    {
        public virtual Api Api { get; set; }

        public virtual DateTime DateTime { get; set; }
        public virtual string TimeStamp { get { return null; } set { this.DateTime = new DateTime((long)Convert.ToDouble(value));} }

        public virtual string MarketName { get; set; }
        public virtual string MarketCoinCode { get; set; }
        public virtual string BaseCoinCode { get; set; }

        public virtual decimal? CurrentAsk { get; set; }
        public virtual decimal? CurrentAskVolume { get; set; }
        public virtual decimal? CurrentBid { get; set; }
        public virtual decimal? CurrentBidVolume { get; set; }

        public virtual decimal? LastPrice { get; set; }
        public virtual decimal? LastVolume { get; set; }

        public virtual decimal? LastSell { get; set; }
        public virtual decimal? LastBuy { get; set; }

        public virtual decimal? High24Hr { get; set; }
        public virtual decimal? Low24Hr { get; set; }
        public virtual decimal? Volume24Hr { get; set; }

        public virtual bool SaveToDb()
        {
            return false;
        }

        public virtual bool SetMarketDetails(string marketName, string marketCoinCode, string baseCoinCode)
        {
            this.MarketName = marketName;
            this.MarketCoinCode = marketCoinCode;
            this.BaseCoinCode = baseCoinCode;
            return true;
        }
    }
}