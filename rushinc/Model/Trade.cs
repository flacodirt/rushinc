﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rushinc.Model
{
    public enum TradeType
    {
        Buy,
        Sell
    }

    public class Trade : DbContext
    {
        public virtual Api Api { get; set; }

        public virtual DateTime DateTime { get; set; }
        public virtual string TimeStamp { get { return null; } set { this.DateTime = new DateTime((long)Convert.ToDouble(value)); } }

        public virtual string MarketName { get; set; }
        public virtual string MarketCoinCode { get; set; }
        public virtual string BaseCoinCode { get; set; }

        public virtual decimal Price { get; set; }
        public virtual decimal Volume { get; set; }

        public virtual TradeType Type { get; set; }
    }
}
