﻿namespace rushinc.Interface
{
    public interface IApi
    {
        string Title { get; set; }
        string Url { get; set; }
        string Username { get; set; }
        string Passphrase { get; set; }
        int CooldownTimeout { get; set; }
    }
}