﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Threading;
using Parse;
using rushinc.Model;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace rushinc.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        #region Properties

        private readonly IDataService _dataService;

        private string windowTitle = string.Empty;
        public string WindowTitle
        {
            get { return windowTitle; }
            set
            {
                if (windowTitle == value) return;
                windowTitle = value;
                RaisePropertyChanged(() => WindowTitle);
            }
        }
        
        private string statusMessage = string.Empty;
        public string StatusMessage
        {
            get { return statusMessage; }
            set
            {
                if (statusMessage == value) return;
                statusMessage = value;
                RaisePropertyChanged(() => StatusMessage);
            }
        }

        private string lastUpdateTime;
        public string LastUpdateTime
        {
            get { return lastUpdateTime; }
            set
            {
                if (lastUpdateTime == value) return;
                lastUpdateTime = value;
                RaisePropertyChanged(() => LastUpdateTime);
            }
        }

        private bool autoRefreshChecked;
        public bool AutoRefreshChecked
        {
            get { return autoRefreshChecked; }
            set
            {
                if (autoRefreshChecked == value) return;
                autoRefreshChecked = value;
                RaisePropertyChanged(() => AutoRefreshChecked);

                if (value)
                {
                    StartExecute();
                    return;
                }
                StopExecute();
            }
        }

        private bool topmostChecked;
        public bool TopmostChecked
        {
            get { return topmostChecked; }
            set
            {
                if (topmostChecked == value) return;
                topmostChecked = value;
                RaisePropertyChanged(() => TopmostChecked);
            }
        }

        private bool showInTaskbarChecked;
        public bool ShowInTaskbarChecked
        {
            get { return showInTaskbarChecked; }
            set
            {
                if (showInTaskbarChecked == value) return;
                showInTaskbarChecked = value;
                RaisePropertyChanged(() => ShowInTaskbarChecked);
            }
        }

        private bool miniModeChecked;
        public bool MiniModeChecked
        {
            get { return miniModeChecked; }
            set
            {
                if (miniModeChecked == value) return;
                miniModeChecked = value;
                RaisePropertyChanged(() => MiniModeChecked);
            }
        }

        private bool soundEnabledChecked;
        public bool SoundEnabledChecked
        {
            get { return soundEnabledChecked; }
            set
            {
                if (soundEnabledChecked == value) return;
                soundEnabledChecked = value;
                RaisePropertyChanged(() => SoundEnabledChecked);
            }
        }
        
        private decimal price_BTCUSD;
        public decimal Price_BTCUSD
        {
            get { return price_BTCUSD; }
            set
            {
                if (value == price_BTCUSD) return;
                price_BTCUSD = value;
                RaisePropertyChanged(() => Price_BTCUSD);
            }
        }

        private decimal price_LTCUSD;
        public decimal Price_LTCUSD
        {
            get { return price_LTCUSD; }
            set
            {
                if (value == price_LTCUSD) return;
                price_LTCUSD = value;
                RaisePropertyChanged(() => Price_LTCUSD);
            }
        }

        private decimal currentAsk_LTCUSD;
        public decimal CurrentAsk_LTCUSD
        {
            get { return currentAsk_LTCUSD; }
            set
            {
                if (value == currentAsk_LTCUSD) return;
                currentAsk_LTCUSD = value;
                RaisePropertyChanged(() => CurrentAsk_LTCUSD);
            }
        }

        private decimal currentBid_LTCUSD;
        public decimal CurrentBid_LTCUSD
        {
            get { return currentBid_LTCUSD; }
            set
            {
                if (value == currentBid_LTCUSD) return;
                currentBid_LTCUSD = value;
                RaisePropertyChanged(() => CurrentBid_LTCUSD);
            }
        }

        private decimal currentAsk_BTCUSD;
        public decimal CurrentAsk_BTCUSD
        {
            get { return currentAsk_BTCUSD; }
            set
            {
                if (value == currentAsk_BTCUSD) return;
                currentAsk_BTCUSD = value;
                RaisePropertyChanged(() => CurrentAsk_BTCUSD);
            }
        }

        private decimal currentBid_BTCUSD;
        public decimal CurrentBid_BTCUSD
        {
            get { return currentBid_BTCUSD; }
            set
            {
                if (value == currentBid_BTCUSD) return;
                currentBid_BTCUSD = value;
                RaisePropertyChanged(() => CurrentBid_BTCUSD);
            }
        }

        private decimal lastSell_POTBTC;
        public decimal LastSell_POTBTC
        {
            get { return lastSell_POTBTC; }
            set
            {
                if (value == lastSell_POTBTC) return;
                lastSell_POTBTC = value;
                RaisePropertyChanged(() => LastSell_POTBTC);
            }
        }

        private decimal lastBuy_POTBTC;
        public decimal LastBuy_POTBTC
        {
            get { return lastBuy_POTBTC; }
            set
            {
                if (value == lastBuy_POTBTC) return;
                lastBuy_POTBTC = value;
                RaisePropertyChanged(() => LastBuy_POTBTC);
            }
        }

        private decimal lastPrice_POTBTC;
        public decimal LastPrice_POTBTC
        {
            get { return lastPrice_POTBTC; }
            set
            {
                if (value == lastPrice_POTBTC) return;
                lastPrice_POTBTC = value;
                RaisePropertyChanged(() => LastPrice_POTBTC);
            }
        }

        private decimal currentAsk_POTBTC;
        public decimal CurrentAsk_POTBTC
        {
            get { return currentAsk_POTBTC; }
            set
            {
                if (value == currentAsk_POTBTC) return;
                currentAsk_POTBTC = value;
                RaisePropertyChanged(() => CurrentAsk_POTBTC);
            }
        }

        private decimal currentBid_POTBTC;
        public decimal CurrentBid_POTBTC
        {
            get { return currentBid_POTBTC; }
            set
            {
                if (value == currentBid_POTBTC) return;
                currentBid_POTBTC = value;
                RaisePropertyChanged(() => CurrentBid_POTBTC);
            }
        }

        private decimal lastSell_POTLTC;
        public decimal LastSell_POTLTC
        {
            get { return lastSell_POTLTC; }
            set
            {
                if (value == lastSell_POTLTC) return;
                lastSell_POTLTC = value;
                RaisePropertyChanged(() => LastSell_POTLTC);
            }
        }

        private decimal lastBuy_POTLTC;
        public decimal LastBuy_POTLTC
        {
            get { return lastBuy_POTLTC; }
            set
            {
                if (value == lastBuy_POTLTC) return;
                lastBuy_POTLTC = value;
                RaisePropertyChanged(() => LastBuy_POTLTC);
            }
        }

        private decimal lastPrice_LTCBTC;
        public decimal LastPrice_LTCBTC
        {
            get { return lastPrice_LTCBTC; }
            set
            {
                if (value == lastPrice_LTCBTC) return;
                lastPrice_LTCBTC = value;
                RaisePropertyChanged(() => LastPrice_LTCBTC);
            }
        }

        private decimal currentAsk_POTLTC;
        public decimal CurrentAsk_POTLTC
        {
            get { return currentAsk_POTLTC; }
            set
            {
                if (value == currentAsk_POTLTC) return;
                currentAsk_POTLTC = value;
                RaisePropertyChanged(() => CurrentAsk_POTLTC);
            }
        }

        private decimal currentBid_POTLTC;
        public decimal CurrentBid_POTLTC
        {
            get { return currentBid_POTLTC; }
            set
            {
                if (value == currentBid_POTLTC) return;
                currentBid_POTLTC = value;
                RaisePropertyChanged(() => CurrentBid_POTLTC);
            }
        }

        private int updateCooldownCount { get; set; }
        private DispatcherTimer dispatcherTimer { get; set; }

        private bool isBusy;
        public bool IsBusy
        {
            get { return isBusy; }
            set
            {
                if (isBusy == value) return;
                isBusy = value;
                RaisePropertyChanged(() => IsBusy);

                StartCommand.RaiseCanExecuteChanged();
            }
        }

        private bool isWaiting;
        public bool IsWaiting
        {
            get { return isWaiting; }
            set
            {
                if (isWaiting == value) return;
                isWaiting = value;
                RaisePropertyChanged(() => IsWaiting);

                StartCommand.RaiseCanExecuteChanged();
            }
        }

        private bool selectedSell;
        public bool SelectedSell
        {
            get { return selectedSell; }
            set
            {
                if (selectedSell == value) return;
                selectedSell = value;
                RaisePropertyChanged(() => SelectedSell);
            }
        }

        private bool selectedBuy;
        public bool SelectedBuy
        {
            get { return selectedBuy; }
            set
            {
                if (selectedBuy == value) return;
                selectedBuy = value;
                RaisePropertyChanged(() => SelectedBuy);
            }
        }

        private ObservableCollection<ApiResponse> currentMarketList = new ObservableCollection<ApiResponse>();
        public ObservableCollection<ApiResponse> CurrentMarketList
        {
            get { return currentMarketList; }
            set
            {
                if (value == currentMarketList) return;
                currentMarketList = value;
                RaisePropertyChanged(() => CurrentMarketList);
            }
        }

        private bool showMarketList;
        public bool ShowMarketList
        {
            get { return showMarketList; }
            set
            {
                if (showMarketList == value) return;
                showMarketList = value;
                RaisePropertyChanged(() => ShowMarketList);
            }
        }

        #endregion

        #region Commands

        private RelayCommand settingsCommand;
        public RelayCommand SettingsCommand
        {
            get { return settingsCommand ?? (settingsCommand = new RelayCommand(SettingsOpen)); }
        }

        private RelayCommand startCommand;
        public RelayCommand StartCommand
        {
            get { return startCommand ?? (startCommand = new RelayCommand(StartExecute, StartCanExecute)); }
        }

        private RelayCommand stopCommand;
        public RelayCommand StopCommand
        {
            get { return stopCommand ?? (stopCommand = new RelayCommand(StopExecute)); }
        }

        #endregion

        #region Methods

        public MainViewModel(IDataService dataService)
        {
            //_dataService = dataService;
            //_dataService.GetData(HandleDataResults);
            InitialLoad();
        }

        private void HandleDataResults(object results, Exception ex)
        {
            if (ex != null)
            {
                // Error reporting
                return;
            }
            var x = ((DataItem) results).Title;
        }

        public override void Cleanup()
        {
            // Clean up if needed
            base.Cleanup();
        }

        private void InitialLoad()
        {
            WindowTitle = "rushinc - Choose your options and click 'Start' to begin.";
            StatusMessage = "rushinc - Loading...";
            TestDB();

            TopmostChecked = false;
            ShowInTaskbarChecked = true;
            MiniModeChecked = true;
            SoundEnabledChecked = true;
            ShowMarketList = false;
            AutoRefreshChecked = true;
            
            // Not needed if AutoRefreshChecked = true by default
            //GoReady();
            //StartExecute();
        }

        // @TODO
        private void SettingsOpen()
        {
            // APIs (Bitstamp, Cryptorush)
            // DBs (Parse)
        }

        // @TODO Parse
        private async void TestDB()
        {
            LoadLocalSettings();
            //var testObject = new ParseObject("TestObject");
            //testObject["foo"] = "bar";
            //await testObject.SaveAsync();
        }

        private void LoadLocalSettings()
        {
            try
            {
                using (StreamReader sr = new StreamReader("settings.txt"))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        string[] stringSeparators = {"|||"};
                        string[] result = line.Split(stringSeparators, StringSplitOptions.None);
                        var apiName = result[0];
                        var apiUID = result[1];
                        var apiPW = result[2];

                        var a = new Api();
                        a.Username = apiUID;
                        a.Passphrase = apiPW;
                        a.CooldownTimeout = 10;
                        bool isChecked = true;
                        if (apiName.ToUpper().Contains("BITSTAMP"))
                        {
                            a.Title = "Bitstamp";
                            a.Url = "https://www.bitstamp.net/api/ticker/";
                        }
                        else if (apiName.ToUpper().Contains("CRYPTORUSH"))
                        {
                            a.Title = "Crypto Rush";
                            a.Url = "https://cryptorush.in/api.php";
                            isChecked = false;
                        }
                        else if (apiName.ToUpper().Contains("MINTPAL"))
                        {
                            a.Title = "Mint Pal";
                            a.Url = "https://api.mintpal.com/market/summary/";
                        }
                        else if (apiName.ToUpper().Contains("HAVELOCK"))
                        {
                            a.Title = "Havelock";
                            a.Url = "https://www.havelockinvestments.com/r/tickerfull";
                        }
                        else if (apiName.ToUpper().Contains("CRYPTSY"))
                        {
                            a.Title = "Cryptsy";
                            a.Url = "http://pubapi.cryptsy.com/api.php?method=singlemarketdata&marketid=173";
                        }

                        if (ApiList == null) ApiList = new ObservableCollection<ApiListItem>();
                        ApiList.Add(new ApiListItem { Api = a, IsChecked = isChecked, IsEnabled = true });
                    }
                }
            }
            catch (Exception e)
            {
                StatusMessage = "rushinc - The 'settings.txt' file could not be read";
                //Console.WriteLine(e.Message);
            }
        }

        private void GoReady()
        {
            IsWaiting = false;
            IsBusy = false;
            StatusMessage = "rushinc - Ready!";
            WindowTitle = StatusMessage;
        }

        private void StartExecute()
        {
            if (IsBusy) return;
            IsBusy = true;
            StatusMessage = "rushinc - Running...";
            WindowTitle = StatusMessage;
            GetDataFromRemoteTicker();
        }

        private bool StartCanExecute()
        {
            return !IsBusy;
        }

        private void StopExecute()
        {
            AutoRefreshChecked = false;
            if (dispatcherTimer != null) dispatcherTimer.Stop();
            GoReady();
        }

        public class ProcessWithUpdates
        {
            public static ObservableCollection<ApiListItem> ApiList { get; set; } 
            public static ObservableCollection<ApiResponse> ApiResponseList { get; set; }

            public async Task Run(IProgress<string> progress, ObservableCollection<ButtonTemplate> checkList)
            {
                string url;
                ApiResponseList = new ObservableCollection<ApiResponse>();
                await Task.Run(() =>
                {
                    // @TODO Run each query in parallel
                    foreach (var ai in ApiList.Where(li => li.IsChecked))
                    {
                        var a = ai.Api;
                        progress.Report(String.Format("Waiting for response from {0}", a.Title));
                        switch (a.Title)
                        {
                            case "Bitstamp":
                                var x = GetJsonDataFromRemoteUrlAsync<BitstampApiResponse>(a.Url);
                                x.Api = a;
                                ApiResponseList.Add(x);
                                break;
                            case "Crypto Rush":
                                url = a.Url + "?key=" + a.Passphrase + "&id=" + a.Username + "&json=true&get=all";
                                foreach (var r in GetJsonDataListFromRemoteUrlAsync<CryptorushApiResponse>(url))
                                {
                                    r.Api = a;
                                    ApiResponseList.Add(r);
                                }
                                break;
                            case "Havelock":
                                foreach (var r in GetJsonDataListFromRemoteUrlAsyncHavelock(a.Url))
                                {
                                    r.Api = a;
                                    ApiResponseList.Add(r);
                                }
                                break;
                            case "Mint Pal":
                                foreach (var r in GetJsonDataListFromRemoteUrlAsync<MintPalApiResponse>(a.Url))
                                {
                                    r.Api = a;
                                    ApiResponseList.Add(r);
                                }
                                break;
                            case "Cryptsy":
                                foreach (var r in GetJsonDataListFromRemoteUrlAsync<CryptsyApiResponse>(a.Url))
                                {
                                    r.Api = a;
                                    ApiResponseList.Add(r);
                                }
                                break;
                        }
                    }
                });

                //@TODO
                //SaveApiResultsToParse(ApiResponseList, checkList);
            }
        }

        /// <summary>
        /// Market data is sent to Parse cloud database for historical tracking and analysis
        /// </summary>
        /// <param name="apiResponseList"></param>
        private static void SaveApiResultsToParse(ObservableCollection<ApiResponse> apiResponseList, ObservableCollection<ButtonTemplate> checkList)
        {
            try
            {
                foreach (var a in apiResponseList)
                {

                    bool next = false;
                    foreach (var c in checkList)
                    {
                        if (!c.IsChecked
                            && c.Api.Title == a.Api.Title
                            && c.CommandParameter == a.MarketCoinCode + "/" + a.BaseCoinCode)
                        {
                            next = true;
                            break;
                        }
                    }
                    if (next) continue;

                    var o = new ParseObject(a.Api.Title.Replace(" ", "").ToUpper());
                    o["ProgramTimeUTC"] = DateTime.UtcNow;
                    o["DateTimeUTC"] = a.DateTime;
                    o["TimeStampUTC"] = a.TimeStamp;
                    o["MarketName"] = a.MarketName;
                    o["MarketCoinCode"] = a.MarketCoinCode;
                    o["BaseCoinCode"] = a.BaseCoinCode;
                    o["LastPrice"] = a.LastPrice.ToString();
                    o["CurrentAsk"] = a.CurrentAsk.ToString();
                    o["CurrentAskVolume"] = a.CurrentAskVolume.ToString();
                    o["CurrentBid"] = a.CurrentBid.ToString();
                    o["CurrentBidVolume"] = a.CurrentBidVolume.ToString();
                    o["LastBuy"] = a.LastBuy.ToString();
                    o["LastPrice"] = a.LastPrice.ToString();
                    o["LastSell"] = a.LastSell.ToString();
                    o["LastVolume"] = a.LastVolume.ToString();
                    //await
                    o.SaveAsync();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Main method grabbing data and populating UI with results
        /// </summary>
        private async void GetDataFromRemoteTicker()
        {
            ProcessWithUpdates pwp = new ProcessWithUpdates();
            ProcessWithUpdates.ApiList = ApiList;
            await pwp.Run(new Progress<string>(pwp_StatusUpdate), CheckList);

            //var OriginalCurrentMarketList = CurrentMarketList;
            var OriginalCheckList = new ObservableCollection<ButtonTemplate>(CheckList);
            CurrentMarketList.Clear();
            CheckList.Clear();

            foreach (var r in ProcessWithUpdates.ApiResponseList.OrderBy(ar => ar.BaseCoinCode).ThenBy(ar => ar.MarketCoinCode))
            {
                if (r.Api.Title.ToUpper() == "BITSTAMP")
                {
                    // Build checklist on first run, keep checked state on consecutive runs
                    var en = OriginalCheckList.SingleOrDefault(enc => enc.Api.Title == r.Api.Title && enc.CommandParameter == "BTC/USD");

                    // Bitstamp doesn't label market/coin codes
                    bool isChecked = en == null || en.IsChecked;

                    // Add to checklist
                    CheckList.Add(new ButtonTemplate
                    {
                        Api = r.Api,
                        Content = "(BS) " + "BTC/USD",
                        CommandParameter = "BTC/USD",
                        IsChecked = isChecked,
                        IsEnabled = true
                    });

                    // Omit unchecked from live market data
                    if (!isChecked) continue;
                    
                    // Update live market data
                    CurrentMarketList.Add(r);

                    // Special labels
                    Price_BTCUSD = Convert.ToDecimal(r.LastPrice);
                    CurrentAsk_BTCUSD = Convert.ToDecimal(r.CurrentAsk);
                    CurrentBid_BTCUSD = Convert.ToDecimal(r.CurrentBid);
                }
                else if (r.Api.Title.ToUpper() == "HAVELOCK")
                {
                    // Build checklist on first run, keep checked state on consecutive runs
                    var en = OriginalCheckList.SingleOrDefault(enc => enc.Api.Title == r.Api.Title && enc.CommandParameter == r.MarketCoinCode);

                    bool isChecked = en != null && en.IsChecked;

                    if (en == null)
                    {
                        // Default these to checked on first run
                        if (r.MarketCoinCode != null && r.MarketCoinCode.ToUpper().Contains("NEOBEE"))
                        {
                            isChecked = true;
                        }
                    }

                    // Add to checklist
                    CheckList.Add(new ButtonTemplate
                    {
                        Api = r.Api,
                        Content = "(HL) " + r.MarketCoinCode,
                        CommandParameter = r.MarketCoinCode,
                        IsChecked = isChecked,
                        IsEnabled = true
                    });

                    if (!isChecked) continue;

                    // Update live market data
                    CurrentMarketList.Add(r);
                }
                else if (r.Api.Title.ToUpper() == "CRYPTO RUSH")
                {
                    // Build checklist on first run, keep checked state on consecutive runs
                    var en = OriginalCheckList.SingleOrDefault(enc => enc.Api.Title == r.Api.Title && enc.CommandParameter == r.MarketName);

                    bool isChecked = en != null && en.IsChecked;

                    if (en == null)
                    {
                        // Default these to checked on first run
                        if (r.MarketName != null && (r.MarketName.ToUpper().Contains("POT/BTC")
                                                     || r.MarketName.ToUpper().Contains("POT/LTC")
                                                     || r.MarketName.ToUpper().Contains("LTC/BTC")
                                                     || r.MarketName.ToUpper().Contains("BTC/USD")
                                                     || r.MarketName.ToUpper().Contains("CRS/BTC"))
                            )
                        {
                            isChecked = true;
                        }
                    }

                    // Add to checklist
                    CheckList.Add(new ButtonTemplate
                    {
                        Api = r.Api,
                        Content = "(CR) " + r.MarketName,
                        CommandParameter = r.MarketName,
                        IsChecked = isChecked,
                        IsEnabled = true
                    });

                    if (!isChecked) continue;

                    // Update live market data
                    CurrentMarketList.Add(r);
                }
                else if (r.Api.Title.ToUpper() == "MINT PAL")
                {
                    // Build checklist on first run, keep checked state on consecutive runs
                    var en = OriginalCheckList.SingleOrDefault(enc => enc.Api.Title == r.Api.Title && enc.CommandParameter == r.MarketCoinCode.ToUpper() + "/" + r.BaseCoinCode.ToUpper());

                    bool isChecked = en != null && en.IsChecked;

                    if (en == null)
                    {
                        // Default these to checked on first run
                        if (
                            (r.MarketCoinCode.ToUpper().Contains("BTC") && r.BaseCoinCode.ToUpper().Contains("POT"))
                            || (r.MarketCoinCode.ToUpper().Contains("BTC") && r.BaseCoinCode.ToUpper().Contains("LTC"))
                            )
                        {
                            isChecked = true;
                        }
                    }

                    // Add to checklist
                    if (r.MarketCoinCode != null)
                    {
                        CheckList.Add(new ButtonTemplate
                        {
                            Api = r.Api,
                            Content = "(MP) " + r.MarketCoinCode.ToUpper() + "/" + r.BaseCoinCode.ToUpper(),
                            CommandParameter = r.MarketCoinCode.ToUpper() + "/" + r.BaseCoinCode.ToUpper(),
                            IsChecked = isChecked,
                            IsEnabled = true
                        });
                    }

                    if (!isChecked) continue;

                    // Update live market data
                    CurrentMarketList.Add(r);

                    // Special labels
                    if (r.BaseCoinCode == "POT" && r.MarketCoinCode == "BTC")
                    {
                        LastPrice_POTBTC = Convert.ToDecimal(r.LastPrice);
                        //LastBuy_POTBTC = Convert.ToDecimal(r.LastBuy);
                        //LastSell_POTBTC = Convert.ToDecimal(r.LastSell);
                        //CurrentAsk_POTBTC = Convert.ToDecimal(r.CurrentAsk);
                        //CurrentBid_POTBTC = Convert.ToDecimal(r.CurrentBid);
                    }

                    if (r.BaseCoinCode == "LTC" && r.MarketCoinCode == "BTC")
                    {
                        LastPrice_LTCBTC = Convert.ToDecimal(r.LastPrice);
                        //LastBuy_POTLTC = Convert.ToDecimal(r.LastBuy);
                        //LastSell_POTLTC = Convert.ToDecimal(r.LastSell);
                        //CurrentAsk_POTLTC = Convert.ToDecimal(r.CurrentAsk);
                        //CurrentBid_POTLTC = Convert.ToDecimal(r.CurrentBid);
                    }
                }
                else if (r.Api.Title.ToUpper() == "CRYPTSY")
                {
                    // Build checklist on first run, keep checked state on consecutive runs
                    var en = OriginalCheckList.SingleOrDefault(enc => enc.Api.Title == r.Api.Title && enc.CommandParameter == r.MarketCoinCode.ToUpper() + "/" + r.BaseCoinCode.ToUpper());

                    bool isChecked = en != null && en.IsChecked;

                    if (en == null)
                    {
                        // Default these to checked on first run
                        if (
                            (r.MarketCoinCode.ToUpper().Contains("BTC") && r.BaseCoinCode.ToUpper().Contains("POT"))
                            || (r.MarketCoinCode.ToUpper().Contains("BTC") && r.BaseCoinCode.ToUpper().Contains("LTC"))
                            )
                        {
                            isChecked = true;
                        }
                    }

                    // Add to checklist
                    if (r.MarketCoinCode != null)
                    {
                        CheckList.Add(new ButtonTemplate
                        {
                            Api = r.Api,
                            Content = "(CY) " + r.MarketCoinCode.ToUpper() + "/" + r.BaseCoinCode.ToUpper(),
                            CommandParameter = r.MarketCoinCode.ToUpper() + "/" + r.BaseCoinCode.ToUpper(),
                            IsChecked = isChecked,
                            IsEnabled = true
                        });
                    }

                    if (!isChecked) continue;

                    // Update live market data
                    CurrentMarketList.Add(r);

                    // Special labels
                    //if (r.BaseCoinCode == "POT" && r.MarketCoinCode == "BTC")
                    //{
                    //    LastPrice_POTBTC = Convert.ToDecimal(r.LastPrice);
                    //    //LastBuy_POTBTC = Convert.ToDecimal(r.LastBuy);
                    //    //LastSell_POTBTC = Convert.ToDecimal(r.LastSell);
                    //    //CurrentAsk_POTBTC = Convert.ToDecimal(r.CurrentAsk);
                    //    //CurrentBid_POTBTC = Convert.ToDecimal(r.CurrentBid);
                    //}

                    //if (r.BaseCoinCode == "LTC" && r.MarketCoinCode == "BTC")
                    //{
                    //    LastPrice_LTCBTC = Convert.ToDecimal(r.LastPrice);
                    //    //LastBuy_POTLTC = Convert.ToDecimal(r.LastBuy);
                    //    //LastSell_POTLTC = Convert.ToDecimal(r.LastSell);
                    //    //CurrentAsk_POTLTC = Convert.ToDecimal(r.CurrentAsk);
                    //    //CurrentBid_POTLTC = Convert.ToDecimal(r.CurrentBid);
                    //}
                }
            }

            CheckListListView = new ListCollectionView(CheckList);
            if (CheckListListView.GroupDescriptions != null)
                CheckListListView.GroupDescriptions.Add(new PropertyGroupDescription("Api"));

            LastUpdateTime = DateTime.Now.ToLongTimeString();
            GoReady();
            StartCooldownTimer();
        }

        private void pwp_StatusUpdate(string m)
        {
            StatusMessage = m;
        }

        private static T GetJsonDataFromRemoteUrlAsync<T>(string url) where T : new()
        {
            using (WebClient w = new WebClient())
            {
                string json_data = string.Empty;
                try
                {
                    Task<string> a = w.DownloadStringTaskAsync(url);
                    json_data = a.Result;
                    return !string.IsNullOrEmpty(json_data) ? JsonConvert.DeserializeObject<T>(json_data) : new T();
                }
                catch (Exception ex)
                {
                    return new T();
                }
            }
        }

        public class ApiListItem : ViewModelBase
        {
            public Api Api { get; set; }

            private bool isEnabled;
            public bool IsEnabled
            {
                get { return isEnabled; }
                set
                {
                    if (value == isEnabled) return;
                    isEnabled = value;
                    RaisePropertyChanged(() => IsEnabled);
                }
            }

            private bool isChecked;
            public bool IsChecked
            {
                get { return isChecked; }
                set
                {
                    if (value == isChecked) return;
                    isChecked = value;
                    RaisePropertyChanged(() => IsChecked);
                }
            }
        }

        private ObservableCollection<ApiListItem> apiList = new ObservableCollection<ApiListItem>();
        public ObservableCollection<ApiListItem> ApiList
        {
            get { return apiList; }
            set
            {
                if (value == apiList) return;
                apiList = value;
                RaisePropertyChanged(() => ApiList);
            }
        }

        public class ButtonTemplate : ViewModelBase
        {
            public Api Api { get; set; }
            public string ApiTitle { get; set; }

            public string Content { get; set; }
            public string CommandParameter { get; set; }

            private bool isChecked;
            public bool IsChecked
            {
                get { return isChecked; }
                set
                {
                    if (value == isChecked) return;
                    isChecked = value;
                    RaisePropertyChanged(() => IsChecked);
                }
            }

            private bool isEnabled;
            public bool IsEnabled
            {
                get { return isEnabled; }
                set
                {
                    if (value == isEnabled) return;
                    isEnabled = value;
                    RaisePropertyChanged(() => IsEnabled);
                }
            }

            private Style style = Skin.GetStyle("ButtonBoldStyle"); // @TODO
            public Style Style
            {
                get { return style; }
                set
                {
                    if (value == style) return;
                    style = value;
                    RaisePropertyChanged(() => Style);
                }
            }
        }

        private ObservableCollection<ButtonTemplate> checkList = new ObservableCollection<ButtonTemplate>();
        public ObservableCollection<ButtonTemplate> CheckList
        {
            get { return checkList; }
            set
            {
                if (value == checkList) return;
                checkList = value;
                RaisePropertyChanged(() => CheckList);
            }
        }

        private ListCollectionView checkListListView;
        public ListCollectionView CheckListListView
        {
            get { return checkListListView; }
            set
            {
                if (value == checkListListView) return;
                checkListListView = value;
                RaisePropertyChanged(() => CheckListListView);
            }
        }

        private static List<T> GetJsonDataListFromRemoteUrlAsync<T>(string url) where T : new()
        {
            using (WebClient w = new WebClient())
            {
                string json_data = string.Empty;
                try
                {
                    List<T> finalList = new List<T>();
                    Task<string> a = w.DownloadStringTaskAsync(url);
                    json_data = a.Result.Replace("{ ,", "{").Replace("\n", "").Replace("\t", "").Replace("}\"", "},\"");

                    if (json_data.StartsWith("["))
                    {
                        JArray data = JArray.Parse(json_data);
                        foreach (var i in data)
                        {
                            var ba = JsonConvert.DeserializeObject<T>(i.ToString());
                            finalList.Add(ba);
                        }
                    }
                    else
                    {
                        JObject data = JObject.Parse(json_data);

                        // Specific to Cryptsy Api Response
                        if (typeof(CryptsyApiResponse).IsAssignableFrom(typeof(T)))
                        {
                            if ((int)data["success"] != 1) return finalList;
                            var res = data["return"];
                            var markets = res["markets"];
                            foreach (var marketData in markets)
                            {
                                var market = marketData.First();
                                var ba = JsonConvert.DeserializeObject<T>(market.ToString());
                                finalList.Add(ba);
                            }
                            return finalList;
                        }

                        foreach (var i in data)
                        {
                            var aa = i.Key.Split(new string[] { "/" }, StringSplitOptions.None);
                            var marketName = i.Key;
                            var marketCoinCode = aa.ElementAt(1);
                            var baseCoinCode = aa.ElementAt(0);
                            var ba = JsonConvert.DeserializeObject<T>(i.Value.ToString());
                            ba.GetType().GetMethod("SetMarketDetails")
                                .Invoke(ba, new String[] { marketName, marketCoinCode, baseCoinCode });
                            finalList.Add(ba);
                        }
                    }

                    return finalList;
                }
                catch (Exception ex)
                {
                    return new List<T>();
                }
            }
        }

        /// <summary>
        /// Specific to Havelock API response
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        private static List<HavelockApiResponse> GetJsonDataListFromRemoteUrlAsyncHavelock(string url)
        {
            using (WebClient w = new WebClient())
            {
                string json_data = string.Empty;
                try
                {
                    List<HavelockApiResponse> finalList = new List<HavelockApiResponse>();
                    Task<string> a = w.DownloadStringTaskAsync(url);
                    json_data = a.Result.Replace("{ ,", "{").Replace("\n", "").Replace("\t", "").Replace("}\"", "},\"");
                    var data = JObject.Parse(json_data);

                    foreach (var i in data)
                    {
                        var item = JsonConvert.DeserializeObject<HavelockApiResponse>(i.Value.ToString());
                        var marketName = item.MarketName;
                        var marketCoinCode = item.MarketCoinCode;
                        var baseCoinCode = item.BaseCoinCode;
                        item.SetMarketDetails(marketName, marketCoinCode, baseCoinCode);
                        finalList.Add(item);
                    }
                    return finalList;
                }
                catch (Exception ex)
                {
                    return new List<HavelockApiResponse>();
                }
            }
        }

        private void StartCooldownTimer()
        {
            updateCooldownCount = 10;
            //IsBusy = true;
            IsWaiting = true;
            dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += TimeClock_Tick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
            dispatcherTimer.Start();
            TimeClock_Tick(null, null);
        }

        private void TimeClock_Tick(object sender, EventArgs e)
        {
            updateCooldownCount--;

            if (updateCooldownCount <= 0 || !AutoRefreshChecked)
            {
                dispatcherTimer.Stop();
                dispatcherTimer = null;
                
                GoReady();

                if (AutoRefreshChecked)
                {
                    StartExecute();
                }
                
                return;
            }

            StatusMessage = "rushinc - Wait " + updateCooldownCount + " seconds...";
            WindowTitle = StatusMessage;
        }

        #endregion
    }
}