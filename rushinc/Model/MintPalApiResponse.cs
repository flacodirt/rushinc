﻿using System;
using Newtonsoft.Json;

namespace rushinc.Model
{
    public class MintPalApiResponse : ApiResponse
    {
        [JsonProperty(PropertyName = "exchange")]
        public override string MarketCoinCode { get; set; }

        [JsonProperty(PropertyName = "code")]
        public override string BaseCoinCode { get; set; }

        [JsonProperty(PropertyName = "last_price")]
        public override decimal? LastPrice { get; set; }

        [JsonProperty(PropertyName = "24hhigh")]
        public override decimal? High24Hr { get; set; }

        [JsonProperty(PropertyName = "24hlow")]
        public override decimal? Low24Hr { get; set; }

        [JsonProperty(PropertyName = "24hvol")]
        public override decimal? Volume24Hr { get; set; }
    }
}