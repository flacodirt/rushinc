﻿using System;
using Newtonsoft.Json;

namespace rushinc.Model
{
    public class HavelockApiResponse : ApiResponse
    {
        [JsonProperty(PropertyName = "name")]
        public override string MarketName { get; set; }

        [JsonProperty(PropertyName = "symbol")]
        public override string MarketCoinCode { get; set; }

        [JsonProperty(PropertyName = "last")]
        public override decimal? LastPrice { get; set; }

        [JsonProperty(PropertyName = "units")]
        public override decimal? LastVolume { get; set; }
    }
}