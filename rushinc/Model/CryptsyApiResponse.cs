﻿using System;
using Newtonsoft.Json;
using System.Collections.ObjectModel;

namespace rushinc.Model
{
    public class CryptsyApiResponse : ApiResponse
    {
        // Example: LTC/BTC
        [JsonProperty(PropertyName = "label")]
        public override string MarketName { get; set; }

        // Example: BTC
        [JsonProperty(PropertyName = "secondarycode")]
        public override string MarketCoinCode { get; set; }

        // Example: LTC
        [JsonProperty(PropertyName = "primarycode")]
        public override string BaseCoinCode { get; set; }

        [JsonProperty(PropertyName = "recenttrades")]
        public ObservableCollection<Trade> RecentTrades { get; set; }

        [JsonProperty(PropertyName = "sellorders")]
        public ObservableCollection<Order> SellOrders { get; set; }

        // @TODO Clean below
        [JsonProperty(PropertyName = "lasttradeprice")]
        public override decimal? LastPrice { get; set; }

        [JsonProperty(PropertyName = "last_sell")]
        public override decimal? LastSell { get; set; }

        [JsonProperty(PropertyName = "current_ask")]
        public override decimal? CurrentAsk { get; set; }

        [JsonProperty(PropertyName = "current_ask_volume")]
        public override decimal? CurrentAskVolume { get; set; }

        [JsonProperty(PropertyName = "last_buy")]
        public override decimal? LastBuy { get; set; }

        [JsonProperty(PropertyName = "current_bid")]
        public override decimal? CurrentBid { get; set; }

        [JsonProperty(PropertyName = "current_bid_volume")]
        public override decimal? CurrentBidVolume { get; set; }

        [JsonProperty(PropertyName = "highest_24h")]
        public override decimal? High24Hr { get; set; }

        [JsonProperty(PropertyName = "lowest_24h")]
        public override decimal? Low24Hr { get; set; }

        [JsonProperty(PropertyName = "volume_24h")]
        public override decimal? Volume24Hr { get; set; }
    }
}