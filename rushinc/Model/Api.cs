﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using rushinc.Interface;

namespace rushinc.Model
{
    public class Api : IApi
    {
        public string Title { get; set; }
        public string Url { get; set; }
        public string Username { get; set; }
        public string Passphrase { get; set; }
        public int CooldownTimeout { get; set; }
    }
}
