﻿using System.Windows;
using rushinc.Model;
using rushinc.ViewModel;

namespace rushinc
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Closing += (s, e) => ViewModelLocator.Cleanup();

            Skin.ChangeSkin("Base");
        }
    }
}