﻿using System;
using System.Globalization;
using System.Media;
using System.Windows;
using System.Windows.Data;
using Microsoft.Practices.ServiceLocation;
using rushinc.Model;
using rushinc.ViewModel;

namespace rushinc.Converters
{
    public class CurrentBidAskToStyleConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (targetType != typeof(Style))
                throw new InvalidOperationException("The target must be a style");

            if (null == values[0] || null == values[1])
                throw new InvalidOperationException("Missing converter parameters");
            
            decimal currentBid = System.Convert.ToDecimal(values[0]);
            decimal currentAsk = System.Convert.ToDecimal(values[1]);

            if (currentBid > currentAsk)
            {
                var vm = ServiceLocator.Current.GetInstance<MainViewModel>();
                if (vm.SoundEnabledChecked)
                {
                    vm.StatusMessage = "Bid: " + currentBid.ToString() + " > " + "Ask: " + currentAsk.ToString();
                    SoundPlayer wowSound = new SoundPlayer("Sounds/Car Alarm-SoundBible.com-2096911689.wav");
                    wowSound.Play();
                }
                return Skin.GetStyle("Border_RedAlertStyle");
            }

            return Skin.GetStyle("BorderBaseStyle");
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}