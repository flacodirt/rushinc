﻿using System.Windows;
using rushinc.ViewModel;

namespace rushinc
{
    public partial class BalanceView : Window
    {
        public BalanceView()
        {
            InitializeComponent();
            Closing += (s, e) => ViewModelLocator.Cleanup();
        }
    }
}