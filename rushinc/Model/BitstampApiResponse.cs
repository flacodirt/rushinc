﻿using System;
using Newtonsoft.Json;

namespace rushinc.Model
{
    public class BitstampApiResponse : ApiResponse
    {
        [JsonProperty(PropertyName = "timestamp")]
        public override string TimeStamp { get { return null; } set { this.DateTime = new DateTime((long)Convert.ToDouble(value)); } }
        
        [JsonProperty(PropertyName = "last")]
        public override decimal? LastPrice { get; set; }
        
        [JsonProperty(PropertyName = "ask")]
        public override decimal? CurrentAsk { get; set; }
        
        [JsonProperty(PropertyName = "bid")]
        public override decimal? CurrentBid { get; set; }
        
        [JsonProperty(PropertyName = "high")]
        public override decimal? High24Hr { get; set; }
        
        [JsonProperty(PropertyName = "low")]
        public override decimal? Low24Hr { get; set; }
        
        [JsonProperty(PropertyName = "volume")]
        public override decimal? Volume24Hr { get; set; }
    }
}