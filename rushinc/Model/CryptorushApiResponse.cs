﻿using System;
using Newtonsoft.Json;

namespace rushinc.Model
{
    public class CryptorushApiResponse : ApiResponse
    {
        [JsonProperty(PropertyName = "market_name")]
        public override string MarketName { get; set; }

        [JsonProperty(PropertyName = "market_coin_code")]
        public override string MarketCoinCode { get; set; }

        [JsonProperty(PropertyName = "base_coin_code")]
        public override string BaseCoinCode { get; set; }

        [JsonProperty(PropertyName = "last_trade")]
        public override decimal? LastPrice { get; set; }

        [JsonProperty(PropertyName = "last_sell")]
        public override decimal? LastSell { get; set; }

        [JsonProperty(PropertyName = "current_ask")]
        public override decimal? CurrentAsk { get; set; }

        [JsonProperty(PropertyName = "current_ask_volume")]
        public override decimal? CurrentAskVolume { get; set; }

        [JsonProperty(PropertyName = "last_buy")]
        public override decimal? LastBuy { get; set; }

        [JsonProperty(PropertyName = "current_bid")]
        public override decimal? CurrentBid { get; set; }

        [JsonProperty(PropertyName = "current_bid_volume")]
        public override decimal? CurrentBidVolume { get; set; }

        [JsonProperty(PropertyName = "highest_24h")]
        public override decimal? High24Hr { get; set; }

        [JsonProperty(PropertyName = "lowest_24h")]
        public override decimal? Low24Hr { get; set; }

        [JsonProperty(PropertyName = "volume_24h")]
        public override decimal? Volume24Hr { get; set; }
    }
}