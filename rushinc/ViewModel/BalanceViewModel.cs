﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Threading;
using Parse;
using rushinc.Model;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace rushinc.ViewModel
{
    public class BalanceViewModel : ViewModelBase
    {
        #region Properties

        private readonly IDataService _dataService;

        private string windowTitle = string.Empty;
        public string WindowTitle
        {
            get { return windowTitle; }
            set
            {
                if (windowTitle == value) return;
                windowTitle = value;
                RaisePropertyChanged(() => WindowTitle);
            }
        }
        
        private bool topmostChecked;
        public bool TopmostChecked
        {
            get { return topmostChecked; }
            set
            {
                if (topmostChecked == value) return;
                topmostChecked = value;
                RaisePropertyChanged(() => TopmostChecked);
            }
        }

        private bool showInTaskbarChecked;
        public bool ShowInTaskbarChecked
        {
            get { return showInTaskbarChecked; }
            set
            {
                if (showInTaskbarChecked == value) return;
                showInTaskbarChecked = value;
                RaisePropertyChanged(() => ShowInTaskbarChecked);
            }
        }

        private string testText = string.Empty;
        public string TestText
        {
            get { return testText; }
            set
            {
                if (testText == value) return;
                testText = value;
                RaisePropertyChanged(() => TestText);
            }
        }

        #endregion

        #region Commands

        //private RelayCommand startCommand;
        //public RelayCommand StartCommand
        //{
        //    get { return startCommand ?? (startCommand = new RelayCommand(StartExecute, StartCanExecute)); }
        //}

        #endregion

        #region Methods

        public BalanceViewModel(IDataService dataService)
        {
            //_dataService = dataService;
            //_dataService.GetData(HandleDataResults);
            InitialLoad();
        }

        private void HandleDataResults(object results, Exception ex)
        {
            if (ex != null)
            {
                // Error reporting
                return;
            }
            var x = ((DataItem) results).Title;
        }

        ////public override void Cleanup()
        ////{
        ////    // Clean up if needed

        ////    base.Cleanup();
        ////}

        private void InitialLoad()
        {
            WindowTitle = "rushinc - Balances";
            TestDB();
            TopmostChecked = true;
            ShowInTaskbarChecked = true;
        }

        // @TODO Parse
        private async void TestDB()
        {
            LoadFakeData();
            //var testObject = new ParseObject("TestObject");
            //testObject["foo"] = "bar";
            //await testObject.SaveAsync();
        }

        public static List<Api> ApiList { get; set; }

        private void LoadFakeData()
        {
            try
            {
                TestText = "AAAHHH";
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }

        #endregion
    }
}