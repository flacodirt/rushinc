﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace rushinc.Model
{
    public static class Util
    {
        public static DateTime UnixTimeStampToDateTime(string unixTimeStamp)
        {
            return UnixTimeStampToDateTime(Convert.ToDouble(unixTimeStamp));
        }

        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }

        private static string version;
        public static string Version
        {
            get
            {
                if (version != null) return version;
                Assembly assembly = Assembly.GetExecutingAssembly();
                if (File.Exists(assembly.Location))
                {
                    FileVersionInfo fileVersion =
                        FileVersionInfo.GetVersionInfo(assembly.Location);
                    version = fileVersion.FileVersion;
                }
                else
                {
                    //Log.AddLogEntry("Unable to read Version!", LogCategory.Error, LogStatus.Failure);
                    version = "?";
                }
                return version;
            }
        }

        /// <summary>
        /// Returns path to %LOCALAPPDATA%\ROIPOS\Log.txt file
        /// </summary>
        /// <returns></returns>
        public static string GetAppDataLogPath()
        {
            string appDataLog = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                @"rushinc\Log.txt");
            string logPath = Path.GetDirectoryName(appDataLog);
            if (!Directory.Exists(logPath))
                Directory.CreateDirectory(logPath);
            return appDataLog;
        }

        /// <summary>
        /// Returns path of executing assembly (as string)
        /// </summary>
        /// <param name="directory"></param>
        /// <returns></returns>
        public static string GetWorkingPath(string directory)
        {
            string dir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            Debug.Assert(dir != null, "dir != null");
            return Path.Combine(dir, directory);
        }

        /// <summary>
        /// Returns path of executing assembly (as DirectoryInfo)
        /// </summary>
        /// <param name="directory"></param>
        /// <returns></returns>
        public static DirectoryInfo GetWorkingPathDirectoryInfo(string directory)
        {
            string dir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            Debug.Assert(dir != null, "dir != null");
            return new DirectoryInfo(Path.Combine(dir, directory));
        }
    }
}