﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Media;

namespace rushinc.Model
{
    public class Skin
    {
        public const string SkinFolder = @"Skins";

        public static ObservableCollection<string> GetSkins()
        {
            ObservableCollection<string> themeNames = new ObservableCollection<string>();
            DirectoryInfo[] themeDirectories = Util.GetWorkingPathDirectoryInfo(SkinFolder).GetDirectories();
            foreach (DirectoryInfo t in themeDirectories)
            {
                themeNames.Add(t.Name);
            }
            return themeNames;
        }

        public struct ResourceDictEntry
        {
            public int DicIndex { get; set; }
            public string Key { get; set; }
            public string Value { get; set; }
        }

        public static string CurrentSkin { get; set; }

        public static void ChangeSkin(string themeName)
        {
            //Util.LogMessage("Skin.ChangeSkin: CurrentSkin=" + CurrentSkin + " NewSkin=" + themeName);
            if (!string.IsNullOrEmpty(CurrentSkin) && CurrentSkin == themeName)
            {
                //Util.LogMessage("No change in theme, returning");
                return;
            }

            CurrentSkin = themeName;

            string desiredSkin = themeName;

            Uri uri;
            ResourceDictionary resourceDict;
            ResourceDictionary finalDict = new ResourceDictionary();

            // Clear then load Base theme
            Application.Current.Resources.MergedDictionaries.Clear();
            themeName = "Base";
            string basePath = Path.Combine(SkinFolder, themeName);
            if (!Directory.Exists(basePath))
            {
                //Log.AddLogEntry("NO THEME DIRECTORY EXISTS: " + basePath, LogCategory.Error, LogStatus.Failure);
                return;
            }
            foreach (FileInfo themeFile in Util.GetWorkingPathDirectoryInfo(basePath).GetFiles())
            {
                //Util.LogMessage("Loading base theme file: " + themeFile.FullName);
                uri = new Uri(Util.GetWorkingPath(Path.Combine(SkinFolder, themeName + @"\" + themeFile.Name)));
                resourceDict = new ResourceDictionary { Source = uri };
                foreach (DictionaryEntry de in resourceDict)
                {
                    finalDict.Add(de.Key, de.Value);
                }
            }
            // If all you want is Base, we are done
            if (desiredSkin == "Base")
            {
                Application.Current.Resources.MergedDictionaries.Add(finalDict);
                //Util.LogMessage("Base theme loaded.");
                return;
            }

            // Now load desired custom theme, replacing keys found in Base theme with new values, and adding new key/values that didn't exist before
            themeName = desiredSkin;
            //Util.LogMessage("ChangeSkin (2): " + themeName);
            string customPath = Path.Combine(SkinFolder, themeName);
            if (!Directory.Exists(customPath))
            {
                Application.Current.Resources.MergedDictionaries.Add(finalDict);
                return;
            }
            foreach (FileInfo themeFile in Util.GetWorkingPathDirectoryInfo(customPath).GetFiles())
            {
                //Util.LogMessage("Loading custom theme file: " + themeFile.FullName);
                uri = new Uri(Util.GetWorkingPath(Path.Combine(SkinFolder, themeName + @"\" + themeFile.Name)));
                resourceDict = new ResourceDictionary { Source = uri };
                foreach (DictionaryEntry x in resourceDict)
                {
                    bool found = false;
                    // Replace existing values
                    if (finalDict.Cast<DictionaryEntry>().Any(z => x.Key.ToString() == z.Key.ToString()))
                    {
                        finalDict[x.Key] = x.Value;
                        found = true;
                    }

                    // Otherwise add new values
                    if (!found)
                    {
                        finalDict.Add(x.Key, x.Value);
                    }
                }
            }

            // Apply final dictionary
            Application.Current.Resources.MergedDictionaries.Add(finalDict);
            //Util.LogMessage("Custom theme loaded successfully.");
        }

        public static Style GetStyle(string styleName)
        {
            foreach (ResourceDictionary rd in Application.Current.Resources.MergedDictionaries)
            {
                foreach (DictionaryEntry de in rd)
                {
                    if (de.Key.ToString() == styleName)
                    {
                        return de.Value as Style;
                    }
                }
            }
            return null;
        }

        public static Brush GetBrush(string styleName)
        {
            foreach (ResourceDictionary rd in Application.Current.Resources.MergedDictionaries)
            {
                foreach (DictionaryEntry de in rd)
                {
                    if (de.Key.ToString() == styleName)
                    {
                        return de.Value as Brush;
                    }
                }
            }
            return null;
        }
    }
}