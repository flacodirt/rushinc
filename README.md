# Info
Simple C#/WPF application to integrate with multiple cryptocurrency markets.
  
# Features
##v0.0.0
 - [x] API support for: Bitstamp, Crypto Rush, Mint Pal, Havelock
 - [x] Market checklist dynamically loaded from each API
 - [x] Ticker: built in delay to prevent being blocked
 - [x] Options: Topmost, Show in Taskbar, Mini mode, Sound enabled, Auto Update
 - [x] Sound Alerts: if largest bid > smallest ask; alert (you'd be surprised how often this happens)
  
# Todo (priority)
 - [ ] (2) Options dialog
 - [ ] (3) Parallel async API commands
 - [ ] (4) Trend alerts (x volume/y period, x trades/y period)
 - [ ] (5) Alerts for arbitrage opportunities (auto profit calculator with settings)
 - [ ] (6) Portfolio summary, Open Orders (read only)
 - [ ] (7) Portfolio management (initiate trades)
 - [ ] (8) Charts/graphs (bar chart, order book graph)
 - [ ] (9) Order book analysis, alerts
 - [ ] (9) UI design
 - [ ] (9) Custom sound alerts
 - [ ] (9) Dark theme
 - [ ] (9) Android App
  
# License
See LICENSE... spoiler alert: MIT (Open Source!)
  
# Contact
info@awoms.com